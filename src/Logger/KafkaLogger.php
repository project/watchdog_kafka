<?php

namespace Drupal\watchdog_kafka\Logger;

use RdKafka\Producer;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LogMessageParserInterface;
use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\Core\Logger\RfcLogLevel;
use Psr\Log\LoggerInterface;

/**
 * KafkaLogger.
 *
 * To produce watchdog logs.
 */
class KafkaLogger implements LoggerInterface {

  use RfcLoggerTrait;

  /**
   * A configuration object containing syslog settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The message's placeholders parser.
   *
   * @var \Drupal\Core\Logger\LogMessageParserInterface
   */
  protected $parser;

  /**
   * Kafka producer object.
   *
   * @var \RdKafka\Producer
   */
  protected $kafkaProducer;

  /**
   * Constructs a SysLog object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory object.
   * @param \Drupal\Core\Logger\LogMessageParserInterface $parser
   *   The parser to use when extracting message variables.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LogMessageParserInterface $parser) {
    $this->config = $config_factory->get('watchdog_kafka.settings');
    $this->parser = $parser;
    $this->kafkaProducer = new Producer();
  }

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public function log($level, $message, array $context = []) {
    global $base_url;
    $error_level = \Drupal::config('system.logging')->get('error_level');
    if ($error_level == ERROR_REPORTING_HIDE) {
      \Drupal::logger('WATCHDOG_KAFKA')->error("Enable error messages to display");
      return;
    }
    if ($error_level == ERROR_REPORTING_DISPLAY_SOME) {
      $allowed_log_types = ['Error', 'Warning'];
    }
    else {
      $allowed_log_types = [
        'Error',
        'Warning',
        'Notice',
        'Emergency',
        'Alert',
        'Critical',
        'Info',
        'Debug',
      ];
    }
    $severity = RfcLogLevel::getLevels();
    if (
      !empty($brokers = $this->config->get('broker'))
      && in_array($severity[$level], $allowed_log_types)
    ) {
      try {
        if ($this->kafkaProducer->addBrokers($brokers) < 1) {
          \Drupal::logger('kafka')->error("Failed adding brokers");
        }
        $topic = $this->kafkaProducer->newTopic($this->config->get('topic'));
        if (!$this->kafkaProducer->getMetadata(FALSE, $topic, 2000)) {
          \Drupal::logger('exception')
            ->error("Failed to get metadata, is broker down?");
        }
        // Populate the message placeholders then replace them in the message.
        $message_placeholders = $this->parser->parseMessagePlaceholders($message, $context);
        $message = empty($message_placeholders) ? $message : strtr($message, $message_placeholders);

        $queue_logs = strtr($this->config->get('format'), [
          '!base_url' => $base_url,
          '!timestamp' => $context['timestamp'],
          '!severity' => $severity[$level],
          '!type' => $context['channel'],
          '!ip' => $context['ip'],
          '!request_uri' => $context['request_uri'],
          '!referer' => $context['referer'],
          '!uid' => $context['uid'],
          '!link' => strip_tags($context['link']),
          '!message' => strip_tags($message),
          '!loglevel' => $error_level,
        ]);
        $topic->produce(RD_KAFKA_PARTITION_UA, 0, $queue_logs);
      }
      catch (\Exception $e) {
        \Drupal::logger('WATCHDOG_KAFKA')
          ->error('Exception in ' . $e->getMessage());
      }
    }
  }

}
