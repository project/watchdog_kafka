<?php

namespace Drupal\watchdog_kafka\Form;

use RdKafka\Producer;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Defines a form that configures kafka settings.
 */
class KafkaConfiguration extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'kafka_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('watchdog_kafka.settings');
    $error_level = $this->config('system.logging')->get('error_level');

    if ($error_level == ERROR_REPORTING_HIDE) {
      $link = Url::fromRoute('system.logging_settings')->toString();
      drupal_set_message(t('Sending of logs depends on the default errors and logging configuration. Please <a href="@link">click here</a> to enable error logging.', ['@link' => $link]), 'warning');
    }

    $form['broker'] = [
      '#type' => 'textarea',
      '#title' => t('Brokers'),
      '#required' => TRUE,
      '#default_value' => $config->get('broker'),
      '#description' => t('List of kafka brokers. Please enter comma separated values.'),
    ];
    $form['topic'] = [
      '#type' => 'textfield',
      '#title' => t('Topic'),
      '#required' => TRUE,
      '#default_value' => $config->get('topic') ?? 'watchdog',
      '#open' => TRUE,
    ];
    $form['format'] = [
      '#type' => 'textfield',
      '#title' => t('Log Format'),
      '#default_value' => $config->get('format') ?? '!base_url | !timestamp | !severity | !type|!ip | !request_uri |!referer | !uid | !link | !message | !loglevel',
      '#open' => TRUE,
      '#description' => t('This field defines the format in which logs will be diplayed on Kafka. The variables can be changed as per the requirements.'),
    ];
    $form['connection'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Kafka Connection'),
      '#open' => TRUE,
    ];
    $form['connection']['submit'] = [
      '#type' => 'submit',
      '#name' => 'test_connection',
      '#value' => $this->t('Test Connection'),
      '#submit' => ['::kafkaConnSubmissionHandler'],
    ];

    $form['apigee']['submit'] = [
      '#type' => 'submit',
      '#name' => 'submit',
      '#value' => $this->t('Save Configuration'),
    ];
    return $form;
  }

  /**
   * Test kafka connection handler.
   */
  public function kafkaConnSubmissionHandler(array &$form, FormStateInterface $form_state) {
    $broker = $form_state->getValue('broker');
    if (!empty($broker)) {
      $producer = new Producer();
      if ($producer->addBrokers($broker) < 1) {
        $this->messenger()->addError("Failed adding brokers.");
        return;
      }
      else {
        $this->messenger()->addStatus("Brokers added successfully..");
        return;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->configFactory()->getEditable('watchdog_kafka.settings');
    $config
      ->set('broker', $values['broker'])
      ->set('topic', $values['topic'])
      ->set('format', $values['format'])
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'watchdog_kafka.settings',
    ];
  }

}
