CONTENTS OF THIS FILE
---------------------

 * Introductionn
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

 * This module will publish the watchdog logs to the Kafka queue.
 * Instead of passing all the logs to the database, Kafka queue and 
   then these messages can be consumed or read by the consumer.



 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/watchdog_kafka

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/node/add/project-issue/watchdog_kafka

REQUIREMENTS
-------------------

 * PHP 7.2 and above, libkafka v0.11.0, php extension rdkafka-3.0.3

INSTALLATION
------------

 * Install as usual, see
   https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8 for further
   information.

CONFIGURATION
-------------
 * To configure this module, go to /admin/config/development/kafka-config
 * Add Kafka brokers.
 * Add Kafka Topic.
 * And save the configuration.
 * You can test the Kafka connection by clicking the "Test Connection" button.

MAINTAINERS
-----------

Current maintainers:

 * Rahul kumar (https://www.drupal.org/u/rkumar)
 * Biswaranjan Pati (branjansse) (https://www.drupal.org/u/branjansse)
